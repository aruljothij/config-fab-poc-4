# Node environment
NODE_ENV=production
#------------------------------------------------------------------------------
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION START
#
#### E1. Entitlement interface :::: Optional ####
# Possible values: mock | active
# Default: active
# mock: Reads entitlement profile from mock_entl.js in <projectRoot/src> folder
# active: Connects to Entitlement Service
# AUTH_MODE=active
#### E2. System user entitlement :::: Required ####
# Possible values: true | false
# Default: false
# true: Indicates that the component is to be considered a system user in entitlements (eg. Gazetteer)
# false: End user entitlements apply
#
# SYSTEM_USER_AUTH=true
######## E. ENTITLEMENTS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION START
#
#### Q1. Query store parameters used in development/test :::: Optional ####
# Possible values: true | false
# Default: true
# The query store is used as a convenience to load a query/variable set in Graphiql
#
# LOAD_QUERY_STORE=true
#### Q2. Default query set to be loaded :::: Optional ####
# If not provided no query will be loaded
#
# DEFAULT_QUERY_LOAD=accessProfile
######## Q. QUERY STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
#------------------------------------------------------------------------------
######## A. APPLICATION CONFIGURATION ######## ::: SECTION START
#### IMPORTANT: This must correspond to the schema definition ####
#
# A1. Application name used in Quest GraphQL Schema as Interface name :::: Required ####
# APP_NAME_ECM=EntitlementsCacheManager
#### A2. Application code for resolver package ::::  Required ####
# The projection store index name must correspond to this code (with lower casing applied)
# APP_CODE_ECM=ECM
#### A3. Application Connector for resolver package ::::  Required ####
# The projection store connector option to be used
# APP_CONNECTOR_ECM=redis
# APP_ALTCONNECTOR_ECM=elasticSearch
#### A4. Application Connector Protocol for resolver package ::::  Required ####
# The projection store connector protocol option to be used
# NOTE: This will be removed eventually
# APP_CONN_PROTOCOL_ECM=http
# APP_ALTCONN_PROTOCOL_ECM=http
#### A5.If schema build scope is set as base only then do not pick up
# devDependencies for packs other than the root
# This is typically relevant for execution in standalone mode
# and not in aggregated mode
# SCHEMA_BUILD_SCOPE=baseOnly
######## A. APPLICATION CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
# CF_SVC_NAME_ELASTICSEARCH=elasticsearch
#### P2. CF Service Name for specific package :::: Optional ####
# This is only applicable at an individual component package level
# The specific package will bind to the service name specified
# When provided in the Quest configuration serves as an override
#
# Overrides can be provided with a suffix
# ECM - EntitlementsService
# ACCT - AccountServices
# ORG - OrgService
# FXRATE - FxRatesService
# BUSCAL - BusinessCalendarService
# REFDATA - ReferenceDataService
# LIQI - LiquidityICLService
# LIQS - LiquiditySweepsService
# LIQC - LiquidityCommonService
# PAY - PaymentsService
# CNR - CNRService
#
# Examples
# CF_SVC_NAME_ELASTICSEARCH_ECM=entl-els-projection
# CF_SVC_NAME_ELASTICSEARCH_ORG=org-els-projection
#### P3. ES Connection string :::: Optional ####
# This is only used in local development.
# Is not considered when deployed in CF
#
# ES_CONNSTRING_ECM=http://localhost:9200
# ES_CONNSTRING_ENTL=http://:@52.214.49.92:9200
# ES_CONNSTRING_ENTL=http://:@single-elastic-node-lb-2087575466.eu-west-1.elb.amazonaws.com:80
#### P4. ES Server Partition for indexes :::: Optional ####
# Indexes will be created/accessed with this prefix
# e.g. if Partition has a value of "DIT" then the index for Account Services will be "dit.acct"
# NOTE: To be activated
#
ES_PARTITION=quest
#### P5. Default response size (throttle) for Search queries :::: Optional ####
# Search queries will throttle the number of records retrieved based on this value
# The default value is 500 records and can be overridden here
# ES_QUERY_LIMIT=2000
#### P6. Response size (limit) for Aggregate queries :::: Optional ####
# Default value: 50
# Aggregate queries will throttle the number of buckets retrieved based on this value
#
# ES_AGG_QUERY_LIMIT=50
#### P6. Timeout threshold in ms :::: Optional ####
# Default value: 30000 ms
#
# ES_TIMEOUT=120000
######## P. PROJECTION STORE CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## E. REGISTRY CONFIGURATION ######## ::: SECTION START
#
#### E1. Enable discovery :::: Optional ###
# Possible values: true | false
# Default value: true
#
EUREKA_DISCOVERY_ENABLED=true
#### E2. Eureka discovery name :::: Required ####
# Default value: notif-dispatcher
#
EUREKA_DISCOVERY_NAME=entitlements-cache-manager
#### E3. Eureka service name :::: Optional ####
# Default value: digital-registry
# Eureka service name to connect for discovery services
#
# CF_SVC_NAME_DIGITAL_REGISTRY=digital-registry
#### E4. Eureka registration method :::: Optional ####
# Possible values: direct | route
# Default value: route
# Eureka registration supporting
# :: direct: Direct container access registering IP
# :: route: Route is registered
#
EUREKA_REGISTRATION_METHOD=direct
#------------------------------------------------------------------------------
######## L. LOG CONFIGURATION ######## ::: SECTION START
#### L1. Logging level :::: Optional ####
# Supported values are
# error - Only errors are logged
# info - Information logs are enabled
# debug - Debug info is included in the logs
# Default value: info
#
LOG_LEVEL=error
#### L2. Console logging :::: Optional ####
# Directed to standard output
#
CONSOLE_LOG=true
#### L3. Logs directed to file :::: Required ####
# Directed to file
# Default location is /logs/quest.log
#
FILE_LOG=false
#### L4. File name for File Logs :::: Optional ####
# Will be available in logs folder
#
# FILE_LOG_NAME=quest.log
######## L. LOG CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION START
#
#### R1. Include error stack trace :::: Optional ####
# Possible values: true | false
# Default value: false
# Set to true if you require stack traces to be included in logs/ errors section in response
#
# ERROR_STACK_TRACE=false
#### R2. Include error path :::: Optional ####
#
# ERROR_PATH=true
#### R3. Include timer information in meta :::: Optional ####
# NOTE: To be made optional with default value as false
#
# TIMER=true
######## R. RESPONSE PARAMETER CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## S. SECURITY CONFIGURATION ######## ::: SECTION START
#### S1. User claims location :::: Optional ####
# Possible values: bearer | header
# Default value: bearer
# :: bearer: bearer id_token in Authorization header
# :: header: custom igtb_headers - igtb_user/igtb_domain
#
USER_CLAIMS_LOCATION=bearer
#### S2. Validate User token :::: Optional ####
# Possible values: true | false
# Default value: true
# Verify signature and expiry of token
# NOTE: Must be able to toggle both of the above independently
#
USER_TOKEN_VALIDATION=true
#### S3. Verify Shared Secret :::: Required ####
# Possible values: true | false
# Verify shared secret for system inquiries (within the delivery tier)
#
# VERIFY_SHARED_SECRET=true
######## S. SECURITY CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## N. SUBCRIPTIO[N]S CONFIGURATION ######## ::: SECTION START
#### N1. Subscriptions enabled ? :::: Required ####
# Possible values: true | false
# Default value: true
#
# SUBSCRIPTIONS_ENABLED=false
#### N2. Subscription port :::: Optional ####
# Possible values: <Port #> e.g. 4443, 80
# If not specified this will be the same as the App Port
# In CF this is 4443 by default
# In OpenShift - 80
#
# SUBSCRIPTION_PORT=80
#### N3. Redis Enabled :::: Required ####
# Possible values: true/false (default: false)
#
# REDIS_ENABLED=true
#### N4. Redis Parameters :::: Required ####
# Possible values:
# SVC_HOST_REDIS: <Host name/address> If not specified - localhost
# SVC_PWD_REDIS: <Password> If not specified - null
# SVC_PORT_REDIS: <Port #> If not specified - 6379
#
# SVC_HOST_REDIS=
# SVC_PWD_REDIS=
# SVC_PORT_REDIS=
#### N5. Redis scope :::: Required ####
# For consuming application this will usually be either subscriber or publisher
# For Quest this is expected to be subscriber
# Possible values: subscriber/publisher/all
# Default value: subscriber
#
####N6. Other options
REDIS_CONN_RETRIES=25
REDIS_CONN_NAME=quest
REDIS_KEY_PREFIX=
# Connection retry delay in ms
REDIS_RETRY_DELAY=30000
######## N. SUBCRIPTIO[N]S CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION START
#### M1. Dependant packages :::: Optional ####
# NOTE: This will be phased out
#
# DEP_PKGS=true
#### M2. GraphIql enabled :::: Optional ####
# NOTE: This is for development/test usage
#
GRAPHIQL_ENABLED=true
#### M3. Quest route prefix used in GraphIql :::: Optional ####
# Possible value: /quest
# Default value: N/A (empty)
# Required to support Graphiql route in Gatekeeper
#
# QUEST_ROUTE_PREFIX=
#### M4. Apollo engine configuration :::: Optional ####
# APOLLO_ENGINE=false
# APOLLO_ENGINE_KEY=service:prasvenk007-DigitalQuest:SGRBE4lyBnr4yU4R09W-HA
# APOLLO_TRACING=false
# APOLLO_CACHE_CONTROL=false
#### M5. NewRelic APM Configuration :::: Optional ####
# NEW_RELIC_LICENSE_KEY=9a0343bb1dd3aecf0b2da2e92302b503098cec38
#### M6. Development only :::: Optional ####
# If token validation is not required then custom headers can be provided
# for user identity transmission in Request
#
# DEV_USER=DHartm390
# DEV_DOMAIN=premierGroup
#### M8. Greeting on Server Start :::: Optional ####
# GREETING=Welcome
######## M. MISCELLANEOUS CONFIGURATION ######## ::: SECTION END
#------------------------------------------------------------------------------
# Log Event Message
LOG_MESSAGES=false
# PUBLISH_AUDIT_EVENTS=false
# INCLUDE_AUDIT_EVENT_CTX=false
#------------------------------------------------------------------------------
# Authorisation step skipped
# no getAuth call for entitlements
NO_AUTH=true
# Specify entitlement source system URI
ENTL_SOURCE_URI=http://cbxbo-hasura-fab.apps.openshift.intellectdesign.com/v1/graphql
# ENTL_SOURCE_URI=https://158.175.191.171:10123/olive/publisher/gl1wrapper/getuserentitlements
# No source mocking
MOCK_SOURCE=false
#MOCK_SOURCEMODEL=AccessProfile
# Use JSON handling in Redis
REDIS_USETYPE=true
# Entitlement source system active status code
# Used for validating user and corporate status
# ESS_ACTIVESTATUS_CODE=1001
PROFILE_CACHE_EXPIRY_PERIOD=10
# Expiry period for user profile in cache
# PROFILE_CACHE_EXPIRY_PERIOD=30
# No xform for entitlement source system response
ESS_NO_XFORM=true
# ESS/ECM Key constructs for caching
# ECM uses both userName/domainName
ECM_CACHE_KEY=${userName}.${domainName}
# ESS uses only userId
ESS_CACHE_KEY=${userId}
# ESS source version to alter source version in mock mode
# ESS_SOURCE_VERSION=


# ENTL_SOURCE_QUERY="query admin_user_access_profile($domainName: String, $userName: String) {  admin_user_access_profile(where: {domain_name: {_eq: $domainName}, login_id: {_eq: $userName}}) {    domain_guid    domain_id    domain_name    login_id    profile_info    updated_at    user_id  } }"
ENTL_HTTP_METHOD=POST
ENTL_SOURCE_HEADERS=x-hasura-admin-secret:cbxadminsecretkey
ESS_RESPONSE_PATH=data.admin_user_access_profile[0].profile_info


ES_VERSION_SCHEME=7